# desc "Explaining what the task does"
# task :tb_checkout do
#   # Task goes here
# end

namespace :tb_checkout do

  desc "Look for old carts and mark them as abandoned"
  task :check_abandoned => :environment do
    result = TbCheckout::Cart.check_abandoned!
    logger.debug("Marked #{result} shopping carts as abandoned")
  end

  def logger
    @_logger = Logger.new(STDOUT) if @_logger.nil?
    return @_logger
  end

end
