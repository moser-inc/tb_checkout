require 'active_support/deprecation'

module TbCheckout
  module Schema

    def self.included(base)
      ActiveRecord::ConnectionAdapters::Table.send :include, TableDefinition
      ActiveRecord::ConnectionAdapters::TableDefinition.send :include, TableDefinition
      ActiveRecord::ConnectionAdapters::AbstractAdapter.send :include, Statements
      ActiveRecord::Migration::CommandRecorder.send :include, CommandRecorder
    end

    module Statements
      # Add purchasable columns to the given database table
      # 
      def add_tb_checkout_purchasable(table_name)
        add_column(table_name, :description, :string)
        add_column(table_name, :price, :decimal, :precision => 8, :scale => 2)
      end

      # Remove purchasable columns from the given database table
      #
      def remove_tb_checkout_purchasable(table_name)
        remove_column(table_name, :description)
        remove_column(table_name, :price)
      end
    end

    module TableDefinition
      # Add purchasable columns to the given database table
      # 
      def tb_checkout_purchasable
        column(:description, :string)
        column(:price, :decimal, :precision => 8, :scale => 2)
      end
    end

    module CommandRecorder #:nodoc:
      def add_tb_checkout_purchasable(*args)
        record(:add_tb_checkout_purchasable, args)
      end

private

      def invert_add_tb_checkout_purchasable(args)
        [:remove_tb_checkout_purchasable, args]
      end
    end

  end
end
