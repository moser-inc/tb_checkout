require "tb_core"
require "activemerchant"
require "active_merchant/billing/rails"
require "tb_checkout/configuration"
require 'tb_checkout/schema'

module TbCheckout
  class Engine < ::Rails::Engine
    include TbCheckout::Schema

    initializer 'tb_checkout.cart_helpers' do |config|
      ActiveSupport.on_load :spud_application_controller do
        Spud::ApplicationController.send :include, TbCheckout::ControllerHelpers
      end
    end

    initializer 'tb_checkout.admin', :before => 'tb_core.admin' do |config|
      Spud::Core.admin_applications += [{
        :name => 'Shopping Carts',
        :url => '/admin/carts',
        :thumbnail => 'tb_checkout/admin/shopping_carts.png'
      },{
        :name => 'Transactions',
        :url => '/admin/transactions',
        :thumbnail => 'tb_checkout/admin/transactions.png'
      }]
    end

    config.generators do |g|
      g.test_framework :rspec, :fixture => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper true
    end

  end
end
