module TbCheckout
  include ActiveSupport::Configurable
  config_accessor :gateway, :canada_is_real, :card_types, :layout, :cart_lifespan
  self.canada_is_real = true
  self.card_types = [:visa, :master, :discover, :american_express]
  self.layout = nil
  self.cart_lifespan = 3.days
end
