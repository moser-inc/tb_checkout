$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "tb_checkout/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "tb_checkout"
  s.version     = TbCheckout::VERSION
  s.authors     = ["Westlake Design"]
  s.email       = ["greg.woods@moserit.com"]
  s.homepage    = "http://bitbucket.org/moser-inc/tb_checkout"
  s.summary     = "Simple shopping cart and checkout system for Twice Baked"
  s.description = "TB Checkout is a shopping cart and payments system designed for use with Twice Baked and Active Merchant"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"].reject{ |f| f.match(/^spec\/dummy\/(log|tmp)/) }

  s.add_dependency "tb_core", ">= 1.3.0"
  s.add_dependency "activemerchant", "~> 1.46.0"

  s.add_development_dependency "mysql2"
  s.add_development_dependency 'rspec-rails', '~> 3.4.2'
  s.add_development_dependency 'factory_girl_rails', '~> 4.5.0'
  s.add_development_dependency 'database_cleaner', '~> 1.3.0'
  s.add_development_dependency 'simplecov', '~> 0.9.1'
end
