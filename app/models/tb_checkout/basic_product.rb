module TbCheckout
  class BasicProduct < ActiveRecord::Base
    self.table_name = 'tb_checkout_basic_products'
    include TbCheckout::Purchasable
  end
end
