module TbCheckout
  class CartItem < ActiveRecord::Base
    self.table_name = 'tb_checkout_cart_items'
    belongs_to :cart, :inverse_of => :cart_items, :touch => true
    belongs_to :item, :polymorphic => true

    validates :cart, :item, :presence => true
    validates :quantity, :numericality => {:greater_than => 0}
    before_save :set_item_values
    after_destroy :cleanup_purchasable_item

    def total_price
      return item_price * quantity
    end

    def has_item_url?
      return self.item && self.item.class.url_builder.present?
    end

    def item_url(view)
      return item.tb_checkout_build_url(view)
    end

    def has_detail_view?
      return self.item && self.item.class.cart_detail_view.present?
    end

    def detail_view
      return self.item.class.cart_detail_view
    end

private

    def set_item_values
      if self.item
        self.item_description = self.item.description
        self.item_price = self.item.price
      end
    end

    def cleanup_purchasable_item
      if self.item && self.item.class.delete_on_removal
        self.item.destroy()
      end
    end

  end
end
