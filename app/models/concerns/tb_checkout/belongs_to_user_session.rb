module TbCheckout::BelongsToUserSession
  extend ActiveSupport::Concern

  included do
    belongs_to :spud_user
    validate :spud_user_id_is_not_zero
    validate :has_session_or_user_id
    validates :session_id, :presence => true

    # Return all records where the user matches
    # Should return zero results of the passed user is nil
    #
    scope :for_user, ->(user){
      if user.present?
        where(:spud_user => user)
      else
        none()
      end
    }

    # Return all records where the session_id matches
    # Should return zero results of the passed session_id is nil
    #
    scope :for_session, ->(session_id){
      if session_id.present?
        where(:session_id => session_id)
      else
        none()
      end
    }
  end

  def belongs_to?(user_id:nil, session_id:nil)
    return (self.session_id == session_id) || (self.spud_user_id.present? && self.spud_user_id != 0 && self.spud_user_id == user_id)
  end

private

  def spud_user_id_is_not_zero
    if spud_user_id == 0
      errors.add(:spud_user_id, 'cannot be 0')
    end
  end

  def has_session_or_user_id
    errors.add(:base, 'must have a user or session') if spud_user_id.blank? && session_id.blank?
  end

end
