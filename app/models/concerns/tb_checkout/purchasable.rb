module TbCheckout::Purchasable
  extend ActiveSupport::Concern

  included do
    has_many :cart_items, :class_name => 'TbCheckout::CartItem', :as => :item
    tb_checkout_quantity_is_editable(true)
    tb_checkout_delete_on_removal(false)

    include ActiveSupport::Callbacks
    define_callbacks :capture
    set_callback :capture, :after, :tb_checkout_after_capture!
  end

  module ClassMethods
    attr_reader :quantity_is_editable, :after_capture, :delete_on_removal, :url_builder, :cart_detail_view

    # Configure whether or not the given class of product should support multiple quantities
    # * Default: true
    #
    def tb_checkout_quantity_is_editable(is_editable)
      @quantity_is_editable = is_editable
    end

    # Configure a hook to run after the corresponding transaction is successfully captured
    # Use this to perform any cleanup tasks on the product model
    #
    # Ex:
    #   tb_checkout_after_capture ->(instance){
    #     instance.update_column(:is_captured, true)
    #   }
    #
    def tb_checkout_after_capture(symbol_or_lambda)
      @after_capture = symbol_or_lambda
    end

    # Configure whether or not the given class of product should self destruct when it is removed from the shopping cart
    # For example, this may make sense for a purchasable record that is only used one time
    # * Default: false
    #
    def tb_checkout_delete_on_removal(should_delete)
      @delete_on_removal = should_delete
    end

    # Configure a callback that builds a URL for the given product page
    # 
    # Ex:
    #   tb_checkout_url_builder ->(instance, context){
    #     context.widget_path(instance.id)
    #   }
    def tb_checkout_url_builder(symbol_or_lambda)
      @url_builder = symbol_or_lambda
    end

    # Configure a partial to be displayed in the shopping cart
    #
    def tb_checkout_cart_detail_view(detail_view)
      @cart_detail_view = detail_view
    end
  end

  def tb_checkout_build_url(view)
    url_builder = self.class.url_builder
    if url_builder.is_a? Proc
      return url_builder.call(self, view)
    elsif url_builder.is_a? Symbol
      return send(url_builder, view)
    end
  end

private

  def tb_checkout_after_capture!
    after_capture = self.class.after_capture
    if after_capture.is_a? Proc
      after_capture.call(self)
    elsif after_capture.is_a? Symbol  
      send(after_capture)
    end
  end

end
