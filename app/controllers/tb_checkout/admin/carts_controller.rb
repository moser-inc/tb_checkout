class TbCheckout::Admin::CartsController < Admin::ApplicationController
  belongs_to_spud_app :shopping_carts
  before_action :setup_breadcrumb
  before_action :load_cart, :only => :show

  def index
    @carts = TbCheckout::Cart.order('created_at desc').includes(:cart_items).paginate(:page => params[:page])
    if params[:status]
      if params[:status] == 'completed'
        @carts = @carts.completed()
      elsif params[:status] == 'in-progress'
        @carts = @carts.in_progress()
      elsif params[:status] == 'abandoned'
        @carts = @carts.abandoned()
      end
    end
    respond_with @carts
  end

  def show
    respond_with @cart
  end

private

  def setup_breadcrumb
    add_breadcrumb 'Shopping Carts', tb_checkout_admin_carts_path(:status => params[:status])
  end

  def load_cart
    @cart = TbCheckout::Cart.where(:id => params[:id]).first
    if @cart.blank?
      flash[:error] = "Could not find the requested Shopping Cart"
      redirect_to tb_checkout_admin_carts_path
      return false
    end
  end

end
