class TbCheckout::Admin::TransactionsController < Admin::ApplicationController
  belongs_to_spud_app :transactions
  add_breadcrumb 'Transactions', :tb_checkout_admin_transactions_path
  before_action :load_transaction, :only => :show

  def index
    @transactions = TbCheckout::Transaction.order('created_at desc').paginate(:page => params[:page])
    respond_with @transactions
  end

  def show
    respond_with @transaction
  end

private

  def load_transaction
    @transaction = TbCheckout::Transaction.where(:id => params[:id]).first
    if @transaction.blank?
      flash[:error] = 'Could not find the requested Transaction'
      redirect_to tb_checkout_admin_transactions_path
      return false
    end
  end

end
