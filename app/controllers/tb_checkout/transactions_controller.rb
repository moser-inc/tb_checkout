class TbCheckout::TransactionsController < TbCheckout::ApplicationController

  before_action :load_cart, :only => [:new, :create, :confirm, :capture]

  def index
    @transactions = TbCheckout::Transaction.captured.includes(:cart => :cart_items).paginate(:page => params[:page], :per_page => 10)
    if current_user
      @transactions = @transactions.for_user(current_user)
    else
      @transactions = @transactions.for_session(session.id)
    end
    respond_with @transactions
  end

  def new
    @transaction = @cart.transactions.new()
    if !Rails.env.production?
      # These values will work against the Authorize.Net sandbox environment
      @transaction.card_type = 'visa'
      @transaction.card_number = 4007000000027
      @transaction.card_ccv = 123
      @transaction.card_expiration = Date.today.next_year
    end
  end

  def create
    @transaction = @cart.transactions.new(transaction_params)
    @transaction.session_id = session.id
    if @transaction.save() && @transaction.capture!
      flash[:notice] = 'Thanks! Your payment was collected successfully'
      redirect_to tb_checkout_transaction_path(@transaction)
    else
      if @transaction.response
        flash.now[:error] = @transaction.response.message
      end
      if @transaction.status == TbCheckout::Transaction::Status::FAIL
        @transaction = TbCheckout::Transaction.new(transaction_params)
      end
      render 'new'
    end
  end

  def show
    @transaction = TbCheckout::Transaction.captured.find_by(:id => params[:id])
    if @transaction.present? && @transaction.belongs_to?(user_id:current_user_id, session_id:session.id)
      render 'show'
    else
      flash[:error] = 'The transaction you were looking for could not be found'
      redirect_to tb_checkout_cart_path
    end
  end

private

  def transaction_params
    params.require(:tb_checkout_transaction).permit(:card_type, :card_number, :card_ccv, :card_expiration, 
      :billing_first_name, :billing_last_name, :billing_address_1, :billing_address_2, :billing_city, :billing_state, :billing_postal)
  end

  def load_cart
    @cart = tb_checkout_current_cart
    if @cart.blank? || @cart.is_empty?
      flash[:error] = 'You cannot check out with an empty cart'
      redirect_to tb_checkout_cart_path
      return false
    end
  end

end
