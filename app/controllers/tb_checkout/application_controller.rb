module TbCheckout
  class ApplicationController < ::ApplicationController
    respond_to :html, :json
    layout TbCheckout.config.layout
  end
end
