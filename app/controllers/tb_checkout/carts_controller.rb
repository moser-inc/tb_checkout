class TbCheckout::CartsController < TbCheckout::ApplicationController

  def show
    @cart = tb_checkout_current_cart
    respond_with @cart
  end

end
