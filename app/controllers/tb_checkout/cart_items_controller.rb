class TbCheckout::CartItemsController < TbCheckout::ApplicationController

  before_action :load_cart
  before_action :load_cart_item, :only => [:update, :destroy]

  def create
    @cart_item = @cart.cart_items.create(cart_item_params)
    respond_with @cart_item do |format|
      format.html{
        if @cart_item.errors.any?
          flash[:error] = "An error occurred: \"#{@cart_item.errors.full_messages.first}\""
          redirect_to :back
        else
          flash[:notice] = "Added \"#{@cart_item.item_description}\" to your cart"
          redirect_to tb_checkout_cart_path
        end
      }
    end
  end

  def update
    @cart_item.update_attributes(cart_item_params)
    respond_with @cart_item do |format|
      format.html{
        if @cart_item.errors.any?
          flash[:error] = "An error occurred: \"#{@cart_item.errors.full_messages.first}\""
          redirect_to :back
        else
          flash[:notice] = "Updated \"#{@cart_item.item_description}\" in your cart"
          redirect_to tb_checkout_cart_path
        end
      }
    end
  end

  def destroy
    @cart_item.destroy()
    respond_with @cart_item do |format|
      format.html{
        flash[:notice] = "Removed \"#{@cart_item.item_description}\" from your cart"
        redirect_to tb_checkout_cart_path
      }
    end
  end

private

  def load_cart
    @cart = tb_checkout_current_cart
  end

  def load_cart_item
    @cart_item = @cart.cart_items.where(:id => params[:id]).first
    if @cart_item.blank?
      raise Spud::NotFoundError('Cart Item')
    end
  end

  def cart_item_params
    params.require(:tb_checkout_cart_item).permit(:item_type, :item_id, :quantity)
  end

end
