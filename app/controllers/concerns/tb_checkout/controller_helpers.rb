module TbCheckout::ControllerHelpers
  extend ActiveSupport::Concern

  included do
    helper_method :tb_checkout_current_cart
  end

  def tb_checkout_current_cart
    return nil unless session.id
    return TbCheckout::Cart.find_or_create_by({
      session_id: session.id,
      spud_user_id: current_user.try(:id),
      is_completed: false,
      is_abandoned: false
    })
  end

end
