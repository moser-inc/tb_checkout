module TbCheckout
  module ApplicationHelper

    # Return a link to the user's shopping cart, with the number of items in the text
    #
    # * link_text: The text of the link. Defaults: "My Cart"
    #
    def tb_checkout_cart_link(link_text:'My Cart')
      if tb_checkout_current_cart.present?
        count = tb_checkout_current_cart.cart_items.count
      else
        count = 0
      end
      link_to "#{link_text} (#{count})", tb_checkout_cart_path, :class => 'tb-checkout-cart-link'
    end

    # Return a form used to add a product to the user's shopping cart
    #
    # * product: The product you wish to add. Should be an ActiveRecord model that conforms to TbCheckout::Purchasable
    # * quantity: Whether to display a quantity picker. Defaults to false
    # * submit_text: Text of the submit button. Default: "Add to Cart"
    # * submit_class: CSS class of the submit button. Default: "btn btn-primary"
    # 
    def tb_checkout_add_to_cart(product, quantity: false, submit_text:'Add to Cart', submit_class:'btn btn-primary')
      cart_item = TbCheckout::CartItem.new({
        :item => product,
        :quantity => 1
      })
      return form_for cart_item, :html => {:class => 'tb-checkout-add-to-cart-form'} do |f|
        concat f.hidden_field :item_type
        concat f.hidden_field :item_id
        if quantity
          concat f.number_field :quantity, {:class => 'tb-checkout-add-to-cart-quantity'}
        else
          concat f.hidden_field :quantity
        end
        concat f.submit submit_text, :class => "#{submit_class} tb-checkout-add-to-cart-submit"
      end
    end

    # Return a list of states for use in a form select field
    #
    def tb_checkout_options_for_states()
      us_states = [
        ['Alabama', 'AL'],
        ['Alaska', 'AK'],
        ['Arizona', 'AZ'],
        ['Arkansas', 'AR'],
        ['California', 'CA'],
        ['Colorado', 'CO'],
        ['Connecticut', 'CT'],
        ['Delaware', 'DE'],
        ['District of Columbia', 'DC'],
        ['Florida', 'FL'],
        ['Georgia', 'GA'],
        ['Hawaii', 'HI'],
        ['Idaho', 'ID'],
        ['Illinois', 'IL'],
        ['Indiana', 'IN'],
        ['Iowa', 'IA'],
        ['Kansas', 'KS'],
        ['Kentucky', 'KY'],
        ['Louisiana', 'LA'],
        ['Maine', 'ME'],
        ['Maryland', 'MD'],
        ['Massachusetts', 'MA'],
        ['Michigan', 'MI'],
        ['Minnesota', 'MN'],
        ['Mississippi', 'MS'],
        ['Missouri', 'MO'],
        ['Montana', 'MT'],
        ['Nebraska', 'NE'],
        ['Nevada', 'NV'],
        ['New Hampshire', 'NH'],
        ['New Jersey', 'NJ'],
        ['New Mexico', 'NM'],
        ['New York', 'NY'],
        ['North Carolina', 'NC'],
        ['North Dakota', 'ND'],
        ['Ohio', 'OH'],
        ['Oklahoma', 'OK'],
        ['Oregon', 'OR'],
        ['Pennsylvania', 'PA'],
        ['Puerto Rico', 'PR'],
        ['Rhode Island', 'RI'],
        ['South Carolina', 'SC'],
        ['South Dakota', 'SD'],
        ['Tennessee', 'TN'],
        ['Texas', 'TX'],
        ['Utah', 'UT'],
        ['Vermont', 'VT'],
        ['Virginia', 'VA'],
        ['Washington', 'WA'],
        ['West Virginia', 'WV'],
        ['Wisconsin', 'WI'],
        ['Wyoming', 'WY']
      ]
      if TbCheckout.config.canada_is_real
        return {
          'United States' => us_states,
          'Canada' => [
            ['Alberta', 'AB'],
            ['British Columbia ', 'BC'],
            ['Manitoba', 'MB'],
            ['New Brunswick', 'NB'],
            ['Newfoundland and Labrador', 'NL'],
            ['Nova Scotia', 'NS'],
            ['Northwest Territories', 'NT'],
            ['Nunavut', 'NU'],
            ['Ontario', 'ON'],
            ['Prince Edward Island', 'PE'],
            ['Quebec', 'QC'],
            ['Saskatchewan', 'SK'],
            ['Yukon', 'YT']
          ]
        }
      else
        return us_states
      end
    end

    # Return a list of credit card types for use with a form select
    #
    def tb_checkout_credit_card_types()
      types = [
        ['Visa', 'visa'],
        ['MasterCard', 'master'],
        ['Discover', 'discover'],
        ['American Express', 'american_express']
      ]
      types.select!{ |a, b|
        TbCheckout.config.card_types.include?(b.to_sym)
      }
      return types
    end

  end
end
