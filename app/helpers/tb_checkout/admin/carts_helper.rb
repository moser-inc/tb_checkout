module TbCheckout::Admin::CartsHelper

  def tb_checkout_status_label_for_cart(cart)
    if cart.is_completed?
      content_tag :span, 'Complete', :class => 'label label-success'
    elsif cart.is_abandoned?
      content_tag :span, 'Abandoned', :class => 'label label-inverse'
    else
      content_tag :span, 'In Progress', :class => 'label label-warning'
    end
  end

end
