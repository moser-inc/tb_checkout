module TbCheckout::Admin::TransactionsHelper

  def tb_checkout_status_label_for_transaction(status)
    if status == 'captured'
      cls = 'success'
    elsif status == 'fail'
      cls = 'danger'
    elsif status == 'refunded' || status == 'voided'
      cls = 'warning'
    else
      cls = 'default'
    end
    content_tag :span, status.titleize, :class => "label label-#{cls}"
  end

end
