class CreateTbCheckoutBasicProducts < ActiveRecord::Migration
  def change
    create_table :tb_checkout_basic_products do |t|
      t.tb_checkout_purchasable
      t.timestamps
    end
  end
end
