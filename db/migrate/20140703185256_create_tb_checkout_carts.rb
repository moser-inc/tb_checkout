class CreateTbCheckoutCarts < ActiveRecord::Migration
  def change
    create_table :tb_checkout_carts do |t|
      t.integer :spud_user_id
      t.index :spud_user_id
      t.string :session_id, :limit => 32
      t.boolean :is_completed, :default => false
      t.boolean :is_abandoned, :default => false
      t.timestamps
    end
  end
end
