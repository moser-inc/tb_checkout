# TB Checkout

TB Checkout is a shopping cart engine intended for use with Rails and [Twice Baked](https://bitbucket.org/westlakedesign/tb_core/). The goal of this engine is to make a drop-in tool for shopping carts and transaction management to your existing product database.

TB Checkout is **not** a full-fledged product management system. Instead, our goal is to make it easy for any of your existing ActiveRecord models to fit into the shopping cart and facilite transactions through a payment gateway.

## Requirements

Checkout requires at minimum the following gems:

- Rails 4
- TB Core 1.2
- Active Merchant 1.43

## Payment Gateways

Those familiar with [Active Merchant](http://activemerchant.org) will know that it supports a wide variety of payment gateways. However, it should be noted that TB Checkout is currently being built and tested against [Authorize.net](authorize.net).

More gateways may be added in the future, but we do not intend to cover them all. Pull requests adding support for a particular gateway are welcome.

## Installation & Usage

First, it is recommended you run the steps listed in the "Installation/Usage" section of the [TB Core](https://bitbucket.org/westlakedesign/tb_core/) README file. Then, perform the following steps:

1. Add the following to your Gemfile

        gem 'tb_checkout'

2. Run bundle install
3. Copy in database migrations to your new rails project

        bundle exec rake railties:install:migrations
        rake db:migrate

4. Restart your application

## Configuration

TB Checkout accepts the following configuration options.

    TbCheckout.configure do |config|
      config.gateway = ActiveMerchant::Billing::AuthorizeNetGateway.new({ ... })
      config.card_types = [:visa, :master, :discover, :american_express]
      config.layout = 'application'
      config.canada_is_real = true
      config.cart_lifespan = 3.days
    end

#### gateway

An `ActiveMerchant` gateway. See the [Active Merchant](http://activemerchant.org) documentation for help creating a payment gateway.

#### card_types

An array of acceptable credit card types. Remove any types you do not wish to see in the payment form.

#### layout

The base view layout to be used for all user-facing views. The default value will inherit from your base `ApplicationController` configuration.

#### canada_is_real

Set to true or false in order to show/hide Canadian provinces in the billing address section of the payment form. Defaults true.


#### cart_lifespan

Configures how long a cart lives until it is marked as abandoned. **NOTE:** Carts are not actually marked as abandoned until 

## Purchasable Models

As stated earlier, TB Checkout is not a product management system. You are expected to build your own library of products, categorizations, and so on. Once you have a product model, making it eligible for the shopping cart is easy.

At minimum, a product must have a `price` decimal attribute and a `description` string attribute. You can add these columns to your model using the built-in migration method `add_tb_checkout_purchasable`. Or you could treat these as virtual attributes using instance methods.

    class AddPurchasableToProducts < ActiveRecord::Migration
      def change
        add_tb_checkout_purchasable :products
      end
    end

And in your model you should include the `TbCheckout::Purchasable` concern.

    class Product < ActiveRecord::Base
      include TbCheckout::Purchasable
    end

### Model Configuration

The `TbCheckout::Purchasable` concern also addes a couple class methods for optional configuration.

    class Product < ActiveRecord::Base
      include TbCheckout::Purchasable

      # Disable the quanitity picker in the shopping cart
      tb_checkout_quantity_is_editable false

      # Run this block after a transaction is captured
      tb_checkout_after_capture ->(instance){
        logger.debug 'My transaction was completed!'
      }

      # Alternatively, pass a method name as a symbol
      tb_checkout_after_capture :after_capture

      def after_capture(instance)
        logger.debug 'My transaction was completed!'
      end

      # Delete this object if it is removed from the shopping cart
      tb_checkout_delete_on_removal true

      # Use this helper method to generate a product page url
      tb_checkout_url_builder ->(instance, view){
        view.product_path(product)
      }

      # Configure a partial that you want to render in the shopping cart
      tb_checkout_cart_detail_view '/products/cart_view'
    end

#### tb_checkout_quantity_is_editable

Pass false to remove the ability to modify quantities in the shopping cart. This is useful in situations where it only makes sense to purchase 1 quantity of something, i.e., making a donation.

#### tb_checkout_after_capture

Pass a lambda literal, or a symbol naming the method you wish to call. This hook will be called after a successful transation.

#### tb_checkout_delete_on_removal

Set to `true` if the object should be permanently deleted when it is removed from the shopping cart. This might make sense in a scenario where your purchasable objects are one-time use records, but not if your purchasable objects are reused across many carts. Defaults to `false`.

#### tb_checkout_url_builder

Pass a lambda literal, or a symbol naming the method you wish to call. This method will be called whenever we want to generate a URL to your product.

#### tb_checkout_cart_detail_view

Sometimes you want to render some custom HTML in the row of a shopping cart. This configuration allows you to point to a partial view file in your project. This partial will be passed an `item` local variable.

**NOTE:** This partial will only be rendered after a succesful checkout. In-progress carts will only render the product description.

## View Helpers

The following helper methods are provided for convenience. See the Rdocs for the most up to date information regarding arguments and return values.

#### tb_checkout_current_cart

Returns the currently active shopping cart or nil if no cart is active.

#### tb_checkout_cart_link

    <%= tb_checkout_cart_link() %>

Returns a link to the shopping cart.

#### tb_checkout_add_to_cart

    <%= tb_checkout_add_to_cart(my_product) %>

Returns a button that, when clicked, adds a product to the shopping cart.
