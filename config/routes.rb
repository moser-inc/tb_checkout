Rails.application.routes.draw do

  namespace :tb_checkout, :path => '/' do

    get 'cart' => 'carts#show'
    resources :cart_items, :only => [:create, :update, :destroy]

    resources :transactions, :path => 'orders', :only => [:index, :show]
    resources :transactions, :path => 'checkout', :only => [:new, :create], :path_names => {:new => '/'}

    namespace :admin do
      resources :carts, :only => [:index, :show]
      resources :transactions, :only => [:index, :show]
    end
  end

end
