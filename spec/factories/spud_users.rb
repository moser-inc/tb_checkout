# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :spud_user do
    sequence :email do |n|
      "john.doe.#{n}@example.com"
    end
    sequence :login do |n|
      "johndoe#{n}"
    end
    password "password"
    password_confirmation "password"
    first_name "John"
    last_name "Doe"
  end
end
