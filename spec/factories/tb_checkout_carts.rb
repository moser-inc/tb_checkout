# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tb_checkout_cart, :class => 'TbCheckout::Cart' do
    spud_user
    session_id "8f74e68687a91c13901ae8809e8fdb6b"
    is_completed false
    is_abandoned false

    after(:create) do |cart, evaluator|
      cart.cart_items = create_list(:tb_checkout_cart_item, 3, :cart => cart)
    end
  end
end
