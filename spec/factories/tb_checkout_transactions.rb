# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tb_checkout_transaction, :class => 'TbCheckout::Transaction' do
    association :cart, :factory => :tb_checkout_cart
    spud_user nil
    amount_charged 0
    card_type "visa"
    card_number "4024007152451281"
    session_id "8f74e68687a91c13901ae8809e8fdb6b"

    # The following is NOT a valid cc number and should throw a basic validation error
    trait :with_invalid_card do
        card_number "4024007152451280"
    end

    # The following will trigger a gateway exception in ActiveMerchant::Billing::BogusGateway
    trait :with_exception_card do
        card_number "4556674473967422"
    end

    card_expiration { Date.today.next_year }
    card_ccv 123
    billing_first_name "John"
    billing_last_name "Doe"
    billing_address_1 "21 S Range Line Rd"
    billing_address_2 "STE 200"
    billing_city "Carmel"
    billing_state "IN"
    billing_postal 46032

    trait :captured do
        after(:create) { |t| t.capture! }
    end
  end
end
