# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tb_checkout_basic_product, :class => 'TbCheckout::BasicProduct' do
    price 9.99
    description "A Basic Product"
  end
end
