# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tb_checkout_cart_item, :class => 'TbCheckout::CartItem' do
    association :cart, :factory => :tb_checkout_cart
    association :item, :factory => :tb_checkout_basic_product
    item_description "My Item"
    item_price 9.99
    quantity 1
  end
end
