require 'rails_helper'

RSpec.describe TbCheckout::CartsController, :type => :controller do

  it "should get the current shopping cart" do
    get :show
    expect(response).to be_success
    expect(assigns(:cart)).to_not be_nil
  end

end
