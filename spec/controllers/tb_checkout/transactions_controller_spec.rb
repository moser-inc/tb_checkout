require 'rails_helper'

RSpec.describe TbCheckout::TransactionsController, :type => :controller do
  let(:user) { FactoryGirl.create(:spud_user) }
  let(:cart) { FactoryGirl.create(:tb_checkout_cart, :spud_user => user) }

  setup :activate_authlogic

  describe "While logged out" do
    it "should list transactions for the current session" do
      iterations = 3
      iterations.times do |i|
        cart = FactoryGirl.create(:tb_checkout_cart, :spud_user => nil, :session_id => session.id)
        FactoryGirl.create(:tb_checkout_transaction, :captured, :cart => cart)
      end
      get :index
      expect(response).to be_success
      expect(assigns(:transactions).count).to eq(iterations)
      expect(assigns(:transactions).first.session_id).to eq(session.id)
    end
  end

  describe "While logged in" do
    before(:each) do
      SpudUserSession.create(user)
    end

    it "should list transactions for the current user" do
      iterations = 3
      iterations.times do |i|
        FactoryGirl.create(:tb_checkout_transaction, :captured, :cart => cart)
      end
      get :index
      expect(response).to be_success
      expect(assigns(:transactions).count).to eq(iterations)
      expect(assigns(:transactions).first.spud_user).to eq(user)
    end

    describe "with an empty cart" do
      it "should redirect back to the shopping cart" do
        get :new
        expect(response).to redirect_to(tb_checkout_cart_path)
      end
    end

    describe "with an item in the cart" do
      before(:each) do
        controller.tb_checkout_current_cart.add_to_cart(FactoryGirl.create(:tb_checkout_basic_product))
      end

      it "should show the checkout form" do
        get :new
        expect(response).to be_success
        expect(assigns(@transaction)).to_not be(nil)
      end

      it "should re-render the form if the transaction is invalid" do
        attributes = FactoryGirl.attributes_for(:tb_checkout_transaction, :with_invalid_card)
        post :create, :tb_checkout_transaction => attributes
        expect(subject).to render_template(:new)
      end

      it "should re-render the form if the payment gateway returns an exception" do
        attributes = FactoryGirl.attributes_for(:tb_checkout_transaction, :with_exception_card)
        post :create, :tb_checkout_transaction => attributes
        transaction = TbCheckout::Transaction.last
        expect(transaction.status).to eq(TbCheckout::Transaction::Status::FAIL)
        expect(subject).to render_template(:new)
      end

      it "should redirect if the transaction is valid" do
        post :create, :tb_checkout_transaction => FactoryGirl.attributes_for(:tb_checkout_transaction)
        transaction = TbCheckout::Transaction.last
        expect(transaction).to_not eq(nil)
        expect(response).to redirect_to(tb_checkout_transaction_path(transaction))
      end
    end

    it "should show the transaction detail" do
      transaction = FactoryGirl.create(:tb_checkout_transaction, :captured, :cart => cart, :spud_user => user)
      get :show, :id => transaction.id
      expect(response).to be_success
      expect(assigns(:transaction)).to eq(transaction)
    end
  end

end
