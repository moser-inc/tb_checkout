require 'rails_helper'

shared_examples_for "belongs_to_spud_user_session" do |related_model_factory|

  describe '.for_user' do
    it 'should return all records for the given user'
    it 'should not return any records of the given user is nil'
  end

  describe '.for_session' do
    it 'should return all records for the given session_id'
    it 'should not return any records of the given session_id is nil'
  end

  describe '#belongs_to?' do
    it 'should return true if the user matches'
    it 'should return true if the session_id matches'
    it 'should return false'
  end

end
