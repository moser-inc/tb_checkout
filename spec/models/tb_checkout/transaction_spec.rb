require 'rails_helper'

RSpec.describe TbCheckout::Transaction, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"

  it_behaves_like 'belongs_to_spud_user_session', :tb_checkout_transaction

  let(:transaction) {
    FactoryGirl.create(:tb_checkout_transaction)
  }

  describe '#capture' do
    context 'when successful' do
      it 'makes a purchase' do
        expect {
          transaction.capture!
        }.to change(transaction, :status).to('captured')
      end
      it 'updates the cart' do
        transaction.capture!
        expect(transaction.cart.is_completed).to eq(true)
      end
    end
  end

  describe '#build_credit_card' do
    it 'returns a credit card' do
      result = transaction.send(:build_credit_card)
      expect(result).to be_a(ActiveMerchant::Billing::CreditCard)
      expect(result.first_name).to eq(transaction.billing_first_name)
    end
  end

  describe '#build_purchase_options' do
    it 'returns some options' do
      result = transaction.send(:build_purchase_options)
      expect(result).to be_a(Hash)
      expect(result[:email]).to eq(transaction.cart.spud_user.email)
    end
  end
end
