# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160523163605) do

  create_table "spud_permissions", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.string   "tag",        limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_permissions", ["tag"], name: "index_spud_permissions_on_tag", unique: true, using: :btree

  create_table "spud_role_permissions", force: :cascade do |t|
    t.integer  "spud_role_id",        limit: 4,   null: false
    t.string   "spud_permission_tag", limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_role_permissions", ["spud_permission_tag"], name: "index_spud_role_permissions_on_spud_permission_tag", using: :btree
  add_index "spud_role_permissions", ["spud_role_id"], name: "index_spud_role_permissions_on_spud_role_id", using: :btree

  create_table "spud_roles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_user_settings", force: :cascade do |t|
    t.integer  "spud_user_id", limit: 4
    t.string   "key",          limit: 255
    t.string   "value",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_users", force: :cascade do |t|
    t.string   "first_name",               limit: 255
    t.string   "last_name",                limit: 255
    t.boolean  "super_admin"
    t.string   "login",                    limit: 255,                 null: false
    t.string   "email",                    limit: 255,                 null: false
    t.string   "crypted_password",         limit: 255,                 null: false
    t.string   "password_salt",            limit: 255,                 null: false
    t.string   "persistence_token",        limit: 255,                 null: false
    t.string   "single_access_token",      limit: 255,                 null: false
    t.string   "perishable_token",         limit: 255,                 null: false
    t.integer  "login_count",              limit: 4,   default: 0,     null: false
    t.integer  "failed_login_count",       limit: 4,   default: 0,     null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip",         limit: 255
    t.string   "last_login_ip",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "time_zone",                limit: 255
    t.integer  "spud_role_id",             limit: 4
    t.boolean  "requires_password_change",             default: false
  end

  add_index "spud_users", ["email"], name: "index_spud_users_on_email", using: :btree
  add_index "spud_users", ["login"], name: "index_spud_users_on_login", using: :btree
  add_index "spud_users", ["spud_role_id"], name: "index_spud_users_on_spud_role_id", using: :btree

  create_table "tb_checkout_basic_products", force: :cascade do |t|
    t.string   "description", limit: 255
    t.decimal  "price",                   precision: 8, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tb_checkout_cart_items", force: :cascade do |t|
    t.integer  "cart_id",          limit: 4,                                       null: false
    t.integer  "item_id",          limit: 4,                                       null: false
    t.string   "item_type",        limit: 255,                                     null: false
    t.string   "item_description", limit: 255
    t.decimal  "item_price",                   precision: 8, scale: 2
    t.integer  "quantity",         limit: 4,                           default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tb_checkout_cart_items", ["cart_id"], name: "index_tb_checkout_cart_items_on_cart_id", using: :btree
  add_index "tb_checkout_cart_items", ["item_id", "item_type"], name: "index_tb_checkout_cart_items_on_item_id_and_item_type", using: :btree

  create_table "tb_checkout_carts", force: :cascade do |t|
    t.integer  "spud_user_id", limit: 4
    t.string   "session_id",   limit: 32
    t.boolean  "is_completed",            default: false
    t.boolean  "is_abandoned",            default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tb_checkout_carts", ["spud_user_id"], name: "index_tb_checkout_carts_on_spud_user_id", using: :btree

  create_table "tb_checkout_transactions", force: :cascade do |t|
    t.integer  "cart_id",                limit: 4
    t.string   "status",                 limit: 255,                           default: "pending"
    t.string   "invoice_num",            limit: 255
    t.integer  "gateway_transaction_id", limit: 8
    t.decimal  "amount_charged",                       precision: 8, scale: 2
    t.string   "card_display",           limit: 255
    t.string   "card_type",              limit: 255
    t.string   "billing_first_name",     limit: 255
    t.string   "billing_last_name",      limit: 255
    t.string   "billing_address_1",      limit: 255
    t.string   "billing_address_2",      limit: 255
    t.string   "billing_city",           limit: 255
    t.string   "billing_state",          limit: 255
    t.string   "billing_postal",         limit: 255
    t.text     "response_text",          limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "spud_user_id",           limit: 4
    t.string   "session_id",             limit: 32
  end

  add_index "tb_checkout_transactions", ["cart_id"], name: "index_tb_checkout_transactions_on_cart_id", using: :btree
  add_index "tb_checkout_transactions", ["session_id"], name: "index_tb_checkout_transactions_on_session_id", using: :btree
  add_index "tb_checkout_transactions", ["spud_user_id"], name: "index_tb_checkout_transactions_on_spud_user_id", using: :btree

end
