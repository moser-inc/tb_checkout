# This migration comes from tb_checkout_engine (originally 20140703185258)
class CreateTbCheckoutCartItems < ActiveRecord::Migration
  def change
    create_table :tb_checkout_cart_items do |t|
      t.integer :cart_id, :null => false
      t.integer :item_id, :null => false
      t.string :item_type, :null => false
      t.string :item_description
      t.decimal :item_price, :precision => 8, :scale => 2
      t.integer :quantity, :default => 1
      t.timestamps
    end
    add_index :tb_checkout_cart_items, :cart_id
    add_index :tb_checkout_cart_items, [:item_id, :item_type]
  end
end
