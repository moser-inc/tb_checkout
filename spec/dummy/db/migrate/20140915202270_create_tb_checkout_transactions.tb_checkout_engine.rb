# This migration comes from tb_checkout_engine (originally 20140703185259)
class CreateTbCheckoutTransactions < ActiveRecord::Migration
  def change
    create_table :tb_checkout_transactions do |t|
      t.integer :cart_id
      t.string :status, :default => 'pending'
      t.string :invoice_num
      t.integer :gateway_transaction_id, :limit => 8
      t.decimal :amount_charged, :precision => 8, :scale => 2
      t.string :card_display
      t.string :card_type
      t.string :billing_first_name
      t.string :billing_last_name
      t.string :billing_address_1
      t.string :billing_address_2
      t.string :billing_city
      t.string :billing_state
      t.string :billing_postal
      t.text :response_text
      t.timestamps
    end
    add_index :tb_checkout_transactions, :cart_id
  end
end
