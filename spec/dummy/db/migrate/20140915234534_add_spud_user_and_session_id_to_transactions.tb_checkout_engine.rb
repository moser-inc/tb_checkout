# This migration comes from tb_checkout_engine (originally 20140915202848)
class AddSpudUserAndSessionIdToTransactions < ActiveRecord::Migration
  def change
    change_table :tb_checkout_transactions do |t|
      t.integer :spud_user_id
      t.index :spud_user_id
      t.string :session_id, :limit => 32
      t.index :session_id
    end
  end
end
